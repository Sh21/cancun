package com.cancun.services;

import com.cancun.dto.ReservationDto;
import com.cancun.entities.Reservation;
import org.springframework.http.ResponseEntity;


import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface ReservationService {

    Reservation addOrUpdateReservation(Reservation reservation);
    Optional<Reservation> findReservationById(Long id);
    List<Reservation> findAllReservations();
    ResponseEntity<?> recupererReservations();
    ResponseEntity<?> recupererReservation(Long idReservation);
    ResponseEntity<?> annulerReservation(Long idReservation);
    ResponseEntity<?> modifierReservation(ReservationDto reservationDto);
    ResponseEntity<?> effectuerUneReservation(ReservationDto reservationDto) throws ParseException;
}
