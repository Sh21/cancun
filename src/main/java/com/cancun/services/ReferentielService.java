package com.cancun.services;

import com.cancun.entities.ExpositionReferentiel;
import com.cancun.structure.Equipement;
import com.cancun.structure.OptionSalleDeBain;
import com.cancun.structure.TypeLogement;
import com.cancun.structure.Vue;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ReferentielService {
    public List<ExpositionReferentiel> recupererEquipements(){
        return Arrays.stream(Equipement.values())
                .map(equipement -> new ExpositionReferentiel(equipement.name(), equipement.getLibelle()))
                .collect(Collectors.toList());
    }

    public List<ExpositionReferentiel> recupererVues(){
        return Arrays.stream(Vue.values())
                .map(vue -> new ExpositionReferentiel(vue.name(), vue.getLibelle()))
                .collect(Collectors.toList());
    }

    public List<ExpositionReferentiel> recupererTypeLogements(){
        return Arrays.stream(TypeLogement.values())
                .map(typeLogement -> new ExpositionReferentiel(typeLogement.name(), typeLogement.getLibelle()))
                .collect(Collectors.toList());
    }

    public List<ExpositionReferentiel> recupererOptionsSalleDeBain(){
        return Arrays.stream(OptionSalleDeBain.values())
                .map(optionSalleDeBain -> new ExpositionReferentiel(optionSalleDeBain.name(), optionSalleDeBain.getLibelle()))
                .collect(Collectors.toList());
    }
}
