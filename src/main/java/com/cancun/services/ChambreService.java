package com.cancun.services;

import com.cancun.entities.Chambre;
import org.springframework.http.ResponseEntity;


import java.util.List;
import java.util.Optional;

public interface ChambreService {
    Optional<Chambre> findRoomById(Long id);

    Chambre addOrUpdateRoom(Chambre chambre);
    List<Chambre> saveAll(List<Chambre> chambres);
    List<Chambre> recupererChambres();
    ResponseEntity<?> recupererChambresDisponible();
    ResponseEntity<?> recupererChambre(Long id);
}
