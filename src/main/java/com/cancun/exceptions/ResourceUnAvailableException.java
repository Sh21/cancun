package com.cancun.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FOUND)
public class ResourceUnAvailableException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ResourceUnAvailableException(String message) {
        super(message);
    }

}
