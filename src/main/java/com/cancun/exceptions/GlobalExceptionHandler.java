package com.cancun.exceptions;



import com.cancun.utils.CustumMessage;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    // handleHttpMediaTypeNotSupported : triggers when the JSON is invalid
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {


        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));

        String detail = builder.toString();

        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.BAD_REQUEST, detail, false,"Invalid JSON");

        return ResponseEntityBuilder.build(err);

    }

    // handleHttpMessageNotReadable : triggers when the JSON is malformed
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        String detail = ex.getMessage();


        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.BAD_REQUEST, detail, false, "Malformed JSON request");

        return ResponseEntityBuilder.build(err);
    }

    // handleMethodArgumentNotValid : triggers when @Valid fails
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        String detail = ex.getBindingResult().getFieldError().getDefaultMessage();

        ApiError err = new ApiError(LocalDateTime.now(),
                HttpStatus.BAD_REQUEST,
                detail,
                false,
                "Validation Errors");

        return ResponseEntityBuilder.build(err);
    }

    // handleMissingServletRequestParameter : triggers when there are missing parameters
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        String detail = ex.getParameterName() + " parameter is missing";

        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.BAD_REQUEST, detail, false, "Missing Parameters");

        return ResponseEntityBuilder.build(err);
    }

    // handleMethodArgumentTypeMismatch : triggers when a parameter's type does not match
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      WebRequest request) {
        String detail = CustumMessage.TYPE_INCORRECT;

        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.BAD_REQUEST, detail, false, "Mismatch Type");

        return ResponseEntityBuilder.build(err);
    }

    // handleConstraintViolationException : triggers when @Validated fails
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleConstraintViolationException(Exception ex, WebRequest request) {

//        String detail = ex.getMessage();
        String detail = CustumMessage.CHAMPS_OBLIGATOIRE_VIDE;


        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.BAD_REQUEST, detail, false, "Constraint Violation");

        return ResponseEntityBuilder.build(err);
    }

    // handleResourceNotFoundException : triggers when there is not resource with the specified ID in BDD
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException ex) {

        String detail = ex.getMessage();

        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.NOT_FOUND, detail, false, "Ressource non trouvée");

        return ResponseEntityBuilder.build(err);
    }


    // handleResourceUnAvailableException : triggers when resource is un available
    @ExceptionHandler(ResourceUnAvailableException.class)
    public ResponseEntity<Object> handleResourceUnAvailableException(ResourceUnAvailableException ex) {

        String detail = ex.getMessage();

        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.FOUND, detail, false, "Ressource indisponible");

        return ResponseEntityBuilder.build(err);
    }

    @ExceptionHandler(ResourceAlertException.class)
    public ResponseEntity<Object> handleResourceAlertException(ResourceAlertException ex) {

        String detail = ex.getMessage();

        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.NOT_ACCEPTABLE, detail, false, "Alert condition non respectée");

        return ResponseEntityBuilder.build(err);
    }

    // handleNoHandlerFoundException : triggers when the handler method is invalid
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        String detail = String.format("Could not find the %s method for URL %s", ex.getHttpMethod(), ex.getRequestURL());
        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.BAD_REQUEST, detail, false, "Method Not Found");

        return ResponseEntityBuilder.build(err);

    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        String detail = ex.getLocalizedMessage();

        ApiError err = new ApiError(LocalDateTime.now(), HttpStatus.BAD_REQUEST, detail, false, "Error occurred");

        return ResponseEntityBuilder.build(err);

    }


    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<Object> handleCustomConflictAPIException(ConflictException ex) {
        String detail = ex.getMessage();
        ApiError err = new ApiError(LocalDateTime.now(),  HttpStatus.CONFLICT, detail, false, "Resource Conflict");
        return ResponseEntityBuilder.build(err);
    }


    @ExceptionHandler(value = MultipartException.class)
    public String handleFileUploadingError(MultipartException exception) {
        logger.warn("Failed to upload attachment", exception);
        return exception.getMessage();
    }

}

