package com.cancun.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ExpositionReferentiel {
    private String clef;
    private String valeur;

    public ExpositionReferentiel(String cle, String valeur) {
        this.clef=cle;
        this.valeur=valeur;
    }
}

