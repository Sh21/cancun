package com.cancun.entities.model;

import com.cancun.dto.ChambreDto;
import com.cancun.dto.ChambreResponseDto;
import com.cancun.dto.ReservationDto;
import com.cancun.entities.Chambre;
import com.cancun.entities.Reservation;

import java.text.ParseException;
import java.util.Objects;

public class CustumMockBean {
    private  static  CustumMockBean instance;

    public static synchronized CustumMockBean getInstance(){
        if(Objects.isNull(instance))
            instance = new CustumMockBean();
        return instance;
    }

    public ChambreDto chambreDtoBean(){
        return new ChambreDto(
                1L,
                "Chambre Double",
                "description type logement Chambre Double",
                15,
                true,
                true,
                1950F
        );
    }
    public ReservationDto reservationDtoPlusChambreBean(){
        ChambreDto chambreDto = chambreDtoBean();
        return new  ReservationDto(
                1L,
                "2023-01-16",
                "2023-01-19",
                chambreDto.getId());
    }

    public ReservationDto reservationDtoChambreInexistantBean(){
        return new  ReservationDto(
                1L,
                "2023-01-16",
                "2023-01-19",
                12L);
    }

    public Reservation reservationChambreInexistantBean() throws ParseException {
        ReservationDto reservationDto = reservationDtoChambreInexistantBean();
        Chambre chambre = chambreBean();
        chambre.setId(12L);
        return  new Reservation().builder()
                .id(1L)
                .annule(false)
                .chambre(chambre)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate(reservationDto.getDateReservation())))
                .dateDebut(Common.dateToLocalDateTime(Common.formatStringToDate(reservationDto.getDateReservation())))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate(reservationDto.getDateReservation())))
                .build();
    }


    public Chambre chambreBean(){
        ChambreDto chambreDto = chambreDtoBean();
        return  new Chambre().builder()
                .typeLogement(chambreDto.getTypeLogement())
                .description(chambreDto.getDescription())
                .superficie(chambreDto.getSuperficie())
                .nonFumeur(chambreDto.isNonFumeur())
                .disponible(chambreDto.isDisponible())
                .montant(chambreDto.getMontant())
                .build();
    }
    public Chambre chambreUnAvailableBean(){
        ChambreDto chambreDto = chambreDtoBean();
        return  new Chambre().builder()
                .typeLogement(chambreDto.getTypeLogement())
                .description(chambreDto.getDescription())
                .superficie(chambreDto.getSuperficie())
                .nonFumeur(chambreDto.isNonFumeur())
                .disponible(!chambreDto.isDisponible())
                .montant(chambreDto.getMontant())
                .build();
    }
}
