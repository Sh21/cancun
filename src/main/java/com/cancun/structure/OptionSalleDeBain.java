package com.cancun.structure;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OptionSalleDeBain {
    DOUCHE("Douche"),
    PEIGNOIR("Peignoir"),
    TOILETTES("Toilettes"),
    CHAUSSONS("Chaussons"),
    SECHE_CHEVEUX("Sèche-cheveux");

    private final String libelle;
}
