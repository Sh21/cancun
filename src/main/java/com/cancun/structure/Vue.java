package com.cancun.structure;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Vue {
    VUE_SUR_LE_LAC("Vue sur le lac"),
    VUE_SUR_LE_JARDIN("Vue sur le jardin"),
    VUE_SUR_LA_PICCINE(" Vue sur la piscine"),
    VUE_SUR_LA_MONTAGNE("Vue sur la montagne"),
    VUE_SUR_SITE_INTERNET("Vue sur un site d'intérêt"),
    VUE_SUR_RIVIERE("Vue sur la rivière"),
    VUE_SUR_COUR_INFTERIEURE("Vue sur une cour intérieure");

    private final String libelle;

}
