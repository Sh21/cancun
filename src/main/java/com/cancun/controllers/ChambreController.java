package com.cancun.controllers;


import com.cancun.services.ChambreService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "api/cancun")
@Validated
@RequiredArgsConstructor
@Tag(name = "Chambre")
public class ChambreController {

    @Autowired
    private final ChambreService chambreService;



    @Operation(summary = "Récupérer toutes les chambres stockées dans la base de données.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Details de toutes les chambres",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                    description = "Page non trouvée",
                    content = @Content)
    })
    @GetMapping(path = "/chambres")
    public ResponseEntity<?> recupererChambres() {
       return chambreService.recupererChambresDisponible();
    }

    @Operation(summary = "Obtenir les détails d'une chambre particulière dans la base de données.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = " Détails de la chambre récupérés dans la base de données",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                    description = "Page non trouvée",
                    content = @Content)
    })
    @GetMapping(path = "/chambre")
    public ResponseEntity<?> chambre(@Valid @Parameter(description = "id of room to be searched") @RequestParam long id) {
       return  chambreService.recupererChambre(id);
    }

}
