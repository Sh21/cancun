package com.cancun.controllers;

import com.cancun.entities.ExpositionReferentiel;
import com.cancun.services.ReferentielService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/referentiel")
@Validated
@RequiredArgsConstructor
@Tag(name = "Referentiel")
public class ReferentielController {

    @Autowired
    private ReferentielService referentielService;


    /**
     * Liste des valeures pour le champs concern de event et news
     * @return
     */

    @GetMapping(path = "/equipements")
    public List<ExpositionReferentiel> recupererEquipements(){
        return referentielService.recupererEquipements();
    }

    @GetMapping(path = "/vues")
    public List<ExpositionReferentiel> recupererVues(){
        return referentielService.recupererVues();
    }

    @GetMapping(path = "/OptionSalleDeBains")
    public List<ExpositionReferentiel> recupererOptionsSalleDeBain(){
        return referentielService.recupererOptionsSalleDeBain();
    }


    @GetMapping(path = "/typeLogements")
    public List<ExpositionReferentiel> recupererTypeLogements(){
        return referentielService.recupererTypeLogements();
    }
}
