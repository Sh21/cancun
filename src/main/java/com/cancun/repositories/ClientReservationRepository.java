package com.cancun.repositories;

import com.cancun.entities.ClientReservation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientReservationRepository extends JpaRepository<ClientReservation, Long> {

}
