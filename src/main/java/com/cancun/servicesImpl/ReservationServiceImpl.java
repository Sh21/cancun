package com.cancun.servicesImpl;

import com.cancun.dto.ReservationDto;
import com.cancun.dto.ReservationResponseDto;
import com.cancun.entities.Chambre;
import com.cancun.entities.Reservation;
import com.cancun.entities.model.Common;
import com.cancun.exceptions.ResourceAlertException;
import com.cancun.exceptions.ResourceNotFoundException;
import com.cancun.exceptions.ResourceUnAvailableException;
import com.cancun.repositories.ReservationRepository;
import com.cancun.services.ChambreService;
import com.cancun.services.ReservationService;
import com.cancun.utils.CustumApiResponse;
import com.cancun.utils.CustumMessage;
import com.cancun.utils.CustumStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private final ChambreService chambreService;
    private final ReservationRepository reservationRepository;


    @Override
    public Reservation addOrUpdateReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }


    @Override
    public Optional<Reservation> findReservationById(Long id) {
        return reservationRepository.findById(id);
    }

    @Override
    public List<Reservation> findAllReservations() {
        return reservationRepository.findAll();
    }

    /**
     * Recuperation de toutes les reservations non annulées
     * @return
     */
    @Override
    public ResponseEntity<?> recupererReservations() {
        String message;

        List<Reservation> reservations= findAllReservations().stream()
                .filter(reservation -> !reservation.getAnnule())
                .collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(reservations)) {
            List<ReservationResponseDto> reservationsList = Common.mapList(reservations, ReservationResponseDto.class);
            message = CustumMessage.OPERATION_SUCCESS;
            return ResponseEntity.ok(new CustumApiResponse(reservationsList, message, CustumStatus.OK));
        } else {
            message = CustumMessage.LISTE_VIDE;
            return ResponseEntity.ok(new CustumApiResponse(new ArrayList<>(), message, CustumStatus.NO_CONTENT));
        }
    }

    @Override
    public ResponseEntity<?> recupererReservation(Long idReservation) {
        String message;
        ReservationResponseDto reservationResponseDto;
        if (idReservation <= 0L) {
            message = CustumMessage.SELECTIONNER_RESERVATION;
            throw new ResourceNotFoundException(message);
        }
        Optional<Reservation> foundReservation = findReservationById(idReservation);
        if (!foundReservation.isPresent()) {
            message = CustumMessage.RESERVATION_INTROUVABLE;
            throw new ResourceNotFoundException(message);
        }
        if(foundReservation.get().getAnnule()){
            message = CustumMessage.RESERVATION_DEJA_ANNULER;
            reservationResponseDto =Common.mapObject(foundReservation.get(), ReservationResponseDto.class);
            reservationResponseDto.setChambre(null);
        }else {
            reservationResponseDto =Common.mapObject(foundReservation.get(), ReservationResponseDto.class);
            message = CustumMessage.OPERATION_SUCCESS;
        }
        return ResponseEntity.ok(new CustumApiResponse(reservationResponseDto,message, CustumStatus.OK));
    }

    /**
     * Annulation d'une reservation
     * @param idReservation
     * @return
     */
    @Override
    public ResponseEntity<?> annulerReservation(Long idReservation) {
        String message;
        if (idReservation <= 0L) {
            message = CustumMessage.SELECTIONNER_RESERVATION;
            throw new ResourceAlertException(message);
        }
        Optional<Reservation> foundReservation = findReservationById(idReservation);
        if (!foundReservation.isPresent()) {
            message = CustumMessage.RESERVATION_INTROUVABLE;
            throw new ResourceNotFoundException(message);
        }
        if (foundReservation.get().getAnnule()) {
            message = CustumMessage.RESERVATION_DEJA_ANNULER;
            throw new ResourceUnAvailableException(message);
        }
        foundReservation.get().setAnnule(true);
        foundReservation.get().setDateAnnulation(Common.dateToLocalDateTime(new Date()));
        foundReservation.get().setDateMisAjour(Common.dateToLocalDateTime(new Date()));
        Chambre chambre = foundReservation.get().getChambre();
        chambre.setDisponible(true);
        addOrUpdateReservation(foundReservation.get());
        chambreService.addOrUpdateRoom(chambre);
        return ResponseEntity.ok(new CustumApiResponse(CustumMessage.CANCEL_OPERATION_SUCCESS, CustumStatus.OK));
    }

    @Override
    public ResponseEntity<?> modifierReservation(ReservationDto reservationDto) {
        String message;
        if (reservationDto.getId() <= 0L) {
            message = CustumMessage.SELECTIONNER_RESERVATION;
            throw new ResourceAlertException(message);
        }
        Optional<Reservation> foundReservation = findReservationById(reservationDto.getId());
        if (!foundReservation.isPresent()) {
            message = CustumMessage.RESERVATION_INTROUVABLE;
            throw new ResourceNotFoundException(message);
        }
        Chambre oldRoom = foundReservation.get().getChambre();

        Optional<Chambre> optionalChambre = chambreService.findRoomById(reservationDto.getChambreId());
        if (!optionalChambre.isPresent()) {
            message = CustumMessage.CHAMBRE_INTROUVABLE;
            throw new ResourceNotFoundException(message);
        }
        if (!optionalChambre.get().isDisponible() && oldRoom != optionalChambre.get()) {
            message = CustumMessage.CHAMBRE_INDISPONIBLE;
            throw new ResourceUnAvailableException(message);
        }

        LocalDate currentDate = LocalDate.now();
        LocalDate localDateReservation = LocalDate.parse(reservationDto.getDateReservation());
        LocalDateTime localDateTimeReservation = LocalDateTime.of(localDateReservation, LocalTime.now());
        LocalDate localDateFin = LocalDate.parse(reservationDto.getDateDepart());
        LocalDateTime dateDebut = LocalDateTime.of(localDateReservation, LocalTime.MIN).plusDays(1);
        LocalDateTime dateFin = LocalDateTime.of(localDateFin, LocalTime.MAX).withNano(0);

        long dureeDateReservationEtDateDebut = currentDate.until(localDateReservation, ChronoUnit.DAYS);

        if (dureeDateReservationEtDateDebut > 30) {
            message = CustumMessage.DATE_RESERVATION_SUPERIEUR_A_30_JOURS;
            throw new ResourceAlertException(message);
        }
        long dureeSejour = dateDebut.until(dateFin, ChronoUnit.DAYS);
        if (dureeSejour > 3) {
            message = CustumMessage.SEJOUR_SUPERIEUR_A_03_JOURS;
            throw new ResourceAlertException(message);
        }

        foundReservation.get().setChambre(optionalChambre.get());
        foundReservation.get().setDateMisAjour(Common.dateToLocalDateTime(new Date()));
        foundReservation.get().setDateReservation(localDateTimeReservation);
        foundReservation.get().setDateDebut(dateDebut);
        foundReservation.get().setDateFin(dateFin);
        foundReservation.get().setDureeSejour((int) dureeSejour);
        addOrUpdateReservation(foundReservation.get());
        if (oldRoom != optionalChambre.get()) {
            oldRoom.setDisponible(true);
        }
        optionalChambre.get().setDisponible(false);
        chambreService.saveAll(Arrays.asList(optionalChambre.get(), oldRoom));

        ReservationResponseDto reservationResponseDto = Common.mapObject(foundReservation.get(), ReservationResponseDto.class);
        return ResponseEntity.ok(new CustumApiResponse(reservationResponseDto, CustumMessage.OPERATION_SUCCESS, CustumStatus.OK));
    }

    @Override
    public ResponseEntity<?> effectuerUneReservation(ReservationDto reservationDto) throws ParseException {
        String message;

        Optional<Chambre> optionalChambre = chambreService.findRoomById(reservationDto.getChambreId());
        if (!optionalChambre.isPresent()) {
            message = CustumMessage.CHAMBRE_INTROUVABLE;
            throw new ResourceNotFoundException(message);
        }
        if (!optionalChambre.get().isDisponible()) {
            message = CustumMessage.CHAMBRE_INDISPONIBLE;
            throw new ResourceUnAvailableException(message);
        }


        Reservation reservation = new Reservation();

        LocalDate currentDate = LocalDate.now();
        LocalDate localDateReservation = LocalDate.parse(reservationDto.getDateReservation());
        LocalDateTime localDateTimeReservation = LocalDateTime.of(localDateReservation, LocalTime.now());
        LocalDate localDateFin = LocalDate.parse(reservationDto.getDateDepart());
        LocalDateTime dateDebut = LocalDateTime.of(localDateReservation, LocalTime.MIN).plusDays(1);
        LocalDateTime dateFin = LocalDateTime.of(localDateFin, LocalTime.MAX).withNano(0);

        long dureeDateReservationEtDateDebut = currentDate.until(localDateReservation, ChronoUnit.DAYS);

        if (dureeDateReservationEtDateDebut > 30) {
            message = CustumMessage.DATE_RESERVATION_SUPERIEUR_A_30_JOURS;
            throw new ResourceAlertException(message);
        }
        long dureeSejour = dateDebut.until(dateFin, ChronoUnit.DAYS);
        if (dureeSejour > 3) {
            message = CustumMessage.SEJOUR_SUPERIEUR_A_03_JOURS;
            throw new ResourceAlertException(message);
        }


        reservation.setChambre(optionalChambre.get());
        reservation.setAnnule(false);
        reservation.setDateCreation(Common.dateToLocalDateTime(new Date()));
        reservation.setDateMisAjour(Common.dateToLocalDateTime(new Date()));
        reservation.setDateReservation(localDateTimeReservation);
        reservation.setDateDebut(dateDebut);
        reservation.setDateFin(dateFin);
        reservation.setDureeSejour((int) dureeSejour);


        optionalChambre.get().setDisponible(false);
        addOrUpdateReservation(reservation);
        chambreService.addOrUpdateRoom(optionalChambre.get());
        ReservationResponseDto reservationResponseDto = Common.mapObject(reservation, ReservationResponseDto.class);
        message = CustumMessage.OPERATION_SUCCESS;
        return ResponseEntity.ok(new CustumApiResponse(reservationResponseDto, message, CustumStatus.OK));
    }

}
