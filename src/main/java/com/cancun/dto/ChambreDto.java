package com.cancun.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class ChambreDto {
    private Long id;
    private String typeLogement;
    private String description;
    private  double superficie;
    private boolean nonFumeur;
    private boolean disponible;
    private float montant;
}
