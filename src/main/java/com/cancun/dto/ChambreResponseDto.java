package com.cancun.dto;


import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ChambreResponseDto {
    private Long id;
    private String typeLogement;
    private String description;
    private  double superficie;
    private boolean nonFumeur;
    private boolean disponible;
    private float montant;
}
