package com.cancun.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;


import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReservationResponseDto {

    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "Date reservation", example = "2023-01-06")
    private LocalDateTime dateReservation;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "Date debut", example = "2023-01-06")
    private LocalDateTime dateDebut;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "Date fin", example = "2023-01-06")
    private LocalDateTime dateFin;

    private Boolean annule;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "Date annulation", example = "2023-01-06")
    private LocalDateTime dateAnnulation;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Schema(description = "Date mise à jour", example = "2023-01-06")
    protected LocalDateTime dateMisAjour;
    private int dureeSejour;
    @Schema(description = "Chambre reservé")
    private ChambreResponseDto chambre;

}
