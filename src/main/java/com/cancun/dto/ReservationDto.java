package com.cancun.dto;


import com.cancun.utils.CustumMessage;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class ReservationDto {
    @JsonIgnore
    private Long id;

    @NotBlank(message = CustumMessage.CHAMPS_OBLIGATOIRE_VIDE)
    @NotNull(message = CustumMessage.CHAMPS_OBLIGATOIRE_VIDE)
    @Schema(description = "Date reservation", example = "2023-01-06")
    private String dateReservation;

    @NotBlank(message = CustumMessage.CHAMPS_OBLIGATOIRE_VIDE)
    @NotNull(message = CustumMessage.CHAMPS_OBLIGATOIRE_VIDE)
    @Schema(description = "Date date départ", example = "2023-01-06")
    private String dateDepart;
    @Schema(description = "identifiant de la chambre")
    private Long chambreId;
}
